
import pytest

from tsp import Place
from tsp.algorithms import AntColony, GreedyTSP, Ant

from tsp import Place

def test_greedy_creation(tsp_obj):
    g=GreedyTSP()

    assert g.initial is None

    g=GreedyTSP(initial=Place(3, 1, 1))

    assert g.initial == Place(3, 1, 1)

def test_greedy(tsp_obj):

    g = GreedyTSP(initial=Place(3, 1, 1))

    solution = g.run(tsp_obj)

    assert int(solution['cost']) == 21 #esta no es la solución


def test_aoc(tsp_obj):
    colony = AntColony()

    solution = colony.run(tsp_obj)

    assert solution['cost'] == 19 #esta no es la solución

def test_ant_smell(tsp_obj):
    pass

def test_ant_move(tsp_obj):
    pass

def test_place(tsp_obj):
    colony=AntColony()
    place=Place(0,1,1)
    ant=Ant(colony,place)

    assert ant.current==place


