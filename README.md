# TSP

## Resumen Tarea: Termina el producto de datos TSP

Alumna: Dorely Morales Santiago

Este es el repositorio con mi entregable de la Tarea "Termina el producto de datos TSP" para la materia de Programación para Ciencia de Datos.

He colocado en master el avance de lo que pude completar de la tarea. Sin embargo, también están disponibles para consulta las ramas de los issues que dejé sin terminar siguiendo el feature-branch problem que vimos en clase.

En total, se abrieron 9 issues que corresponden a cada una de las instrucciones a realizar en la tarea. Se pudieron cerrar los issues 1 y 2 pues test_tsp.py pasó las pruebas. Por otro lado, aunque se avanzó en el resto de los puntos de la tarea sobre la clase Ant, AntColony y el código para graficar funciones aún falta corregir errores así como el desarrollo de más pruebas.

## Datos

Los archivos contenidos en la carpeta `data` provienen originalmente
de [National Traveling Salesman Problems](https://www.math.uwaterloo.ca/tsp/world/countries.html).

Pero el formato fué modificado para este proyecto.

| Original                | Nombre               |
|:-----------------------:|:--------------------:|
| ar9162.tsp              | argentina.tsp        |
| att_48.tsp, att48_d.txt | eua.tsp, eua48_d.tsp |
| dantzig42_d.tsp         | dantzig_d.tsp        |
| nu3496.tsp              | nicaragua.tsp        |
| qa194.tsp               | qatar.tsp            |
| ym7663.tsp              | yemen.tsp            |
| p01_d.txt, p01_xy.txt   | p01.tsp, p01_d.tsp   |

Los archivos sin sufijo `_d` contienen las coordenadas. Los que tienen
el sufijo `_d` la matriz de distancia.
