"""
"""
import numpy as np
from scipy.spatial import distance
from dataclasses import dataclass
from typing import List
from more_itertools import pairwise
import matplotlib.pyplot as plt

class TSP:
    """
    Describes a TSP
    """
    def __init__(self, coordinates,distances):
        self.places = [Place(id, v[0], v[1]) for id, v in enumerate(coordinates)]
        self.distances=distances

    def cost(self, place, another_place):
        return self.distances[place.id][another_place.id]

    def total_cost(self, tour):
        total_cost = 0.0
        for current, next in tour:
            total_cost += self.cost(current, next)

        return total_cost

    def missing(self,visited_places):
        return set(self.places)-set(visited_places)

    def plot_cities(self):
        for place in self.places:
            plt.plot(place.x,place.y)
            plt.show()

    #Se copia funcion para graficar la solución

    #def plot_solution(path):
    #    plt.plot(cities[:, 0], cities[:, 1], 'co')
    #    plt.xlim(0, MAX_DISTANCE)
    #    plt.ylim(0, MAX_DISTANCE)

    #    for (_from, to) in pairwise(path):
    #        plt.arrow(cities[_from][0], cities[_from][1],
    #                  cities[to][0] - cities[_from][0], cities[to][1] - cities[_from][1],
    #                  color='b', length_includes_head=True)
        # Close the loop
    #    last = path[-1]
    #    first = path[0]
    #    plt.arrow(cities[last][0], cities[last][1],
    #              cities[first][0] - cities[last][0],
    #              cities[first][1] - cities[last][1],
    #              color='b', length_includes_head=True)
    #    plt.show()

    @classmethod
    def from_coordinates(cls,coordinates_file):
        coordinates = np.loadtxt(coordinates_file)
        distances = distance.cdist(coordinates, coordinates, 'euclidean')
        return cls(coordinates, distances)

    @classmethod
    def from_files(cls,coordinates_file, distances_file):
        coordinates=np.loadtxt(coordinates_file)
        distances=np.loadtxt(distances_file) if distances_file else distance.cdist(coordinates, coordinates, 'euclidean')
        return cls(coordinates,distances)

    @classmethod
    def from_random(cls,num_places=50, max_distance=100):
        coordinates = np.random.randint(low=0, high=max_distance, size=(num_places,2))
        distances = distance.cdist(coordinates, coordinates,'euclidean')
        return cls(coordinates,distances)

    def __str__(self):
        return f"TSP [size: {len(self.places)} places]"

@dataclass(eq=True,frozen=True)
class Place:
    """A place to be visited"""
    id: int
    x: float
    y: float

class Tour:
    """
    """
    def __init__(self):
        self._path = []

    @property
    def initial(self):
        return self._path[0]

    @property
    def current(self):
        return self._path[-1]

    @property
    def closed(self):
        closed = False
        if len(self._path) > 1:
            closed = self._path[0] == self._path[-1]

        return closed

    def close(self):
        if len(self._path) > 1:
            self._path.append(self._path[0])

    def append(self, place):
        self._path.append(place)

    def __len__(self):
        return len(self._path)

    @property
    def visited_places(self):
        return set(self._path)

    def __iter__(self):
        return ((current, next) for (current,next) in pairwise(self._path))


    def __repr__(self):
        return f"Tour: [{'->'.join([str(place) for place in self._path])}]"


    def __str__(self):
        return f"Tour: [{'->'.join([place.id for place in self._path])}]"

__version__ = '0.1.0'


#from .cli import cli
